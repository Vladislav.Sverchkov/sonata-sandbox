Sonata Standard Edition
=======================

Install
--------------
1. clone https://gitlab.com/Vladislav.Sverchkov/sonata-sandbox
2. download https://drive.google.com/open?id=1SjIqD0T3XjUBbjHfpb7pEuRojlUGv9i0 (tar for linux, 7z for windows)
   extract to dir sandbox (for linux: tar -xvf vendor.tar.gz)
3. copy app/config/parameters.yml.dist to app/config/parameters.yml:

    database_driver: pdo_mysql
    database_host: 127.0.0.1
    database_name: sonata_sandbox
    database_user: user
    database_password: pass
    
4. create - database_name: sonata_sandbox
5. php vendor/sensio/distribution-bundle/Sensio/Bundle/DistributionBundle/Resources/bin/build_bootstrap.php
6. php app/console doctrine:schema:create
7. php app/console fos:user:create --super-admin admin admin@domain.com SECRETPASSWORD
8.     php bin/console  sonata:page:create-site --enabled=true --name=**site_name** --locale=uk --host=**site url** --relativePath=/ --enabledFrom=now --enabledTo="+10 years" --default=false
       php bin/console sonata:page:update-core-routes --site=all
       php bin/console sonata:page:create-snapshots --site=all
9. [Setting up or Fixing File Permissions](https://symfony.com/doc/3.3/setup/file_permissions.html)
10. php app/console cache:clear --env=prod
11. Create on Apache or Nginx virtual host? example for Apache:

/etc/apache2/sites-available/localhost.sandbox.it.conf:

<VirtualHost *:80>
    ServerAdmin admin@localhost.sandbox.it
    ServerName localhost.sandbox.it
    ServerAlias localhost.sandbox.it
    DocumentRoot /home/www-data/www/sandbox/web
<Directory /home/www-data/www/sandbox/web>
        Options Indexes FollowSymLinks
        Require all granted
        AllowOverride All

        FallbackResource /app.php
</Directory>
    ErrorLog /home/www-data/www/apache_log/sandbox/error.log
    CustomLog /home/www-data/www/apache_log/sandbox/access.log combined
</VirtualHost>

11. Add localhost.sandbox.it to hosts
12. Restart web server